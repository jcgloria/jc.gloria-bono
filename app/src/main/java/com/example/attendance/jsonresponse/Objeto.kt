package com.example.attendance.jsonresponse


import com.google.gson.annotations.SerializedName

data class Objeto(

	@field:SerializedName("documents")
	val documents: List<DocumentsItem?>? = null,

	@field:SerializedName("nextPageToken")
	val nextPageToken: String? = null
)