package com.example.attendance.jsonresponse


import com.google.gson.annotations.SerializedName


data class Timestamp(

	@field:SerializedName("timestampValue")
	val timestampValue: String? = null
)