package com.example.attendance.jsonresponse


import com.google.gson.annotations.SerializedName


data class Fields(

	@field:SerializedName("code")
	val code: Code? = null,

	@field:SerializedName("name")
	val name: Name? = null,

	@field:SerializedName("mac")
	val mac: Mac? = null,

	@field:SerializedName("timestamp")
	val timestamp: Timestamp? = null
)