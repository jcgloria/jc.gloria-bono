package com.example.attendance.jsonresponse


import com.google.gson.annotations.SerializedName


data class Mac(

	@field:SerializedName("stringValue")
	val stringValue: String? = null
)