package com.example.attendance

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    //var dayFormat = SimpleDateFormat("d")
    //var day = dayFormat.format(System.currentTimeMillis())
    //var monthFormat = SimpleDateFormat("M")
    //var monthIncorrect = Integer.parseInt(monthFormat.format(System.currentTimeMillis()).toString())
    //var monthCorrected = monthIncorrect - 1
    //var month = monthCorrected.toString()
    //var yearFormat = SimpleDateFormat("yyyy")
    //var year = yearFormat.format(System.currentTimeMillis())




    var date:String= ""





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val cal = Calendar.getInstance()
        var day = cal.get(Calendar.DAY_OF_MONTH).toString()
        var month = cal.get(Calendar.MONTH).toString()
        var year = cal.get(Calendar.YEAR).toString()

        date = day + "-" + month + "-" + year
        textView2.text = date
        }

    fun bluetoothOn(view: View){
        val eintent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(eintent, 1)
    }

    fun check(view: View){

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + date + "/students/201531389"
        println(url)
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response -> textView.text = "Se ha registrado la asistencia"
            },
            Response.ErrorListener { textView.text = "No se ha registrado la asistencia" })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

    fun nextActivity(view:View){
        val intent = Intent(applicationContext, Activity2::class.java)
        startActivity(intent)
    }


    }
