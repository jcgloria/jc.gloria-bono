package com.example.attendance

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.example.attendance.jsonresponse.*
import kotlinx.android.synthetic.main.activity_2.*
import java.util.*

import android.widget.AdapterView
import android.widget.AdapterView.*
import android.widget.ArrayAdapter

import android.widget.Toast
import java.io.*


class Activity2 : AppCompatActivity() {

    var date = "" //fecha seleccionada
    var estudiantes = ArrayList<Fields?>() //lista de todos los estudiantes
    var csvInfo = ArrayList<String?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)

        var dates= ArrayList<String>() //lista de clases de mobiles en el semestre
        var reader:BufferedReader = BufferedReader((InputStreamReader(getAssets().open("dates.txt"), "UTF-8")))
        var line = reader.readLine()
        while(line != null){
            dates.add(line)
            line = reader.readLine()
        }

        spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, dates)
        spinner.onItemSelectedListener = object : OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                println("No hay nada seleccionado")
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                date = dates[p2]
            }
        }

        //obtener todos los estudiantes
        var url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=100"
        val queue = Volley.newRequestQueue(this)
        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                var gson = Gson()
                var data: Objeto = gson.fromJson(response, Objeto::class.java)
                for(i in data.documents.orEmpty()){
                    var estudiante = i?.fields
                    estudiantes.add(estudiante)
                }

            },
            Response.ErrorListener { println("Error")})
        queue.add(stringRequest)

    }


    fun goBack(view: View){
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    fun checkAbs(view:View){
        csvInfo = ArrayList<String?>()
        linearLayout.removeAllViews()
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + date + "/students?pageSize=100"
        println(url)

        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                var gson = Gson()
                var data: Objeto = gson.fromJson(response, Objeto::class.java)

                for(j in estudiantes.orEmpty()) { //lista de todos los estudiantes
                    var nombre = j?.name?.stringValue
                    var codigo = j?.code?.stringValue
                    var fueAClase = false

                    for (i in data.documents.orEmpty()) { //lista de los que fueron a clase hoy
                        var codigoActual = i?.fields?.code?.stringValue
                        if (codigoActual.equals(codigo)) {
                            fueAClase = true
                            break
                        }
                    }
                        if (fueAClase == false) {

                            var text = nombre + " - " + codigo
                            var view = TextView(applicationContext)
                            view.text = text
                            view.setTextColor(Color.BLACK)
                            linearLayout.addView(view)

                            csvInfo.add(text)

                        }

                }
            },
            Response.ErrorListener {response ->
                linearLayout.removeAllViews()
                var view = TextView(applicationContext)
                view.text = "Error - Status Code: " + response.networkResponse.statusCode.toString()
                view.setTextColor(Color.BLACK)
                linearLayout.addView(view)

            })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

    fun saveCSV(view:View){
        var info = StringBuilder()
        for(x in csvInfo) {
            info.append(x)
            info.append(", ")
        }
            val file = File(getExternalFilesDir(null), "data.txt")
            val out = FileOutputStream(file)
            out.write(info.toString().toByteArray())
            out.close()
            var toast = Toast.makeText(applicationContext,"Se ha guardado el archivo", Toast.LENGTH_SHORT)
            toast.show()



    }
}
